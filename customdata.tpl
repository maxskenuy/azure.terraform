#!/bin/bash

# Fetch the host's SSH public key and add it to the known_hosts file
ssh-keyscan -p "$port" "$host" >> ~/.ssh/known_hosts

# Update package lists
sudo apt-get update

# Install prerequisite packages
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common gnupg-agent

# Add Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
# Add Docker repository
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu focal stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Update package lists with the Docker repository
sudo apt-get update

# Install Docker and Docker Compose
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Add current user to the 'docker' group to run Docker commands without sudo
sudo usermod -aG docker adminuser

# Enable Docker to start on system boot
sudo systemctl enable docker

# Install Docker Compose
sudo curl -fsSL https://github.com/docker/compose/releases/latest/download/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Acquire docker-compose file through wget
sudo apt-get install wget
sudo wget https://gitlab.com/maxskenuy/azure.terraform/-/raw/main/docker-compose.yml
sudo wget https://gitlab.com/maxskenuy/azure.terraform/-/raw/main/logstash.conf
sudo wget https://gitlab.com/maxskenuy/azure.terraform/-/raw/main/filebeat.yml
# sudo wget https://gitlab.com/maxskenuy/azure.terraform/-/raw/main/.dockerignore
# sudo wget https://gitlab.com/maxskenuy/azure.terraform/-/raw/main/.env
# sudo wget https://gitlab.com/maxskenuy/azure.terraform/-/raw/main/dynamic_conf.yml

#creating directories + preparing docker-file
sudo mkdir /home/adminuser/docker

#filebeat
sudo mkdir /usr/share/filebeat
sudo mv filebeat.yml /home/adminuser/docker/
sudo cp -a /home/adminuser/docker/filebeat.yml /usr/share/filebeat/
sudo chmod -R 755 /var/log/

#mysql 
# sudo mkdir -p /var/lib/ /var/lib/mysql /etc/mysql/conf.d
# sudo mkdir -p /etc/mysql/ /etc/mysql/conf.d/

#logstash
sudo mkdir -p /usr/share/logstash/ /usr/share/logstash/pipeline
sudo mv logstash.conf /home/adminuser/docker/
sudo cp -a /home/adminuser/docker/logstash.conf /usr/share/logstash/pipeline

#docker
# sudo mkdir /home/adminuser/docker
# sudo chmod -R 755 /etc/mysql/conf.d /var/www/html/ /home/adminuser/docker

#wordpress
sudo mkdir -p /var/www/ /var/www/html/

#move to-be-used files to expected locations
mv docker-compose.yml /home/adminuser/docker/
mv .env /home/adminuser/docker/
# mv wp.env /var/www/html/
# mv db.env /etc/mysql/conf.d/

# Creating docker-file dir + preparing docker-file
cd /home/adminuser/docker
sudo chmod +x docker-compose.yml

# Run docker-compose file
sudo docker-compose -f /home/adminuser/docker/docker-compose.yml up -d